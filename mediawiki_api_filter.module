<?php
/**
 * @file
 *
 *
 *
 */

/* ====================== */
/* ==== DRUPAL HOOKS ==== */
/* ====================== */

/**
 * Define content filters.
 *
 * @return
 *   An associative array of filters, whose keys are internal filter names,
 *   which should be unique and therefore prefixed with the name of the module.
 *   Each value is an associative array describing the filter, with the
 *   following elements (all are optional except as noted):
 *   - title: (required) An administrative summary of what the filter does.
 *   - description: Additional administrative information about the filter's
 *     behavior, if needed for clarification.
 *   - settings callback: The name of a function that returns configuration form
 *     elements for the filter. See hook_filter_FILTER_settings() for details.
 *   - default settings: An associative array containing default settings for
 *     the filter, to be applied when the filter has not been configured yet.
 *   - prepare callback: The name of a function that escapes the content before
 *     the actual filtering happens. See hook_filter_FILTER_prepare() for
 *     details.
 *   - process callback: (required) The name the function that performs the
 *     actual filtering. See hook_filter_FILTER_process() for details.
 *   - cache (default TRUE): Specifies whether the filtered text can be cached.
 *     Note that setting this to FALSE makes the entire text format not
 *     cacheable, which may have an impact on the site's overall performance.
 *     See filter_format_allowcache() for details.
 *   - tips callback: The name of a function that returns end-user-facing filter
 *     usage guidelines for the filter. See hook_filter_FILTER_tips() for
 *     details.
 *   - weight: A default weight for the filter in new text formats.
 */
function mediawiki_api_filter_filter_info() {
  $filters['mediawiki_api_filter'] = array(
    'title' => t('MediaWiki API filter'),
    'description' => t('Users can use MediaWiki code formats.'),
    'process callback' => '_mediawiki_api_filter_process',
    'settings callback' => '_mediawiki_api_filter_settings',
    'tips callback' => '_mediawiki_api_filter_tips',
  );

  return $filters;
}
/* ====================== */
/* == MODULE FUNCTIONS == */
/* ====================== */

/**
 * Process callback for hook_filter_info().
 *
 * @param $text
 *   The text string to be filtered.
 * @param $filter
 *   The filter object containing settings for the given format.
 * @param $format
 *   The text format object assigned to the text to be filtered.
 * @param $langcode
 *   The language code of the text to be filtered.
 * @param $cache
 *   A Boolean indicating whether the filtered text is going to be cached in
 *   {cache_filter}.
 * @param $cache_id
 *   The ID of the filtered text in {cache_filter}, if $cache is TRUE.
 *
 * @return
 *   The filtered text.
 */
function _mediawiki_api_filter_process($text, $filter, $format, $langcode, $cache, $cache_id) {

  // Query
  $query = array(
    'action' => 'parse',
    'format' => 'php',
  );

  // Fields
  $fields = array(
    'text' => $text,
  );

  // Init curl request
  $ch = curl_init($filter->settings['mediawiki_api_filter_url'] .'?' . drupal_http_build_query($query));

  curl_setopt($ch, CURLOPT_HEADER, 0);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($ch, CURLOPT_POST, TRUE);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

  // Execute
  $res = curl_exec($ch);

  curl_close($ch);

  $data = unserialize($res);
  return $data['parse']['text']['*'];
}

/**
 * Settings callback for hook_filter_info().
 *
 * @param $form
 *   The prepopulated form array of the filter administration form.
 * @param $form_state
 *   The state of the (entire) configuration form.
 * @param $filter
 *   The filter object containing the current settings for the given format,
 *   in $filter->settings.
 * @param $format
 *   The format object being configured.
 * @param $defaults
 *   The default settings for the filter, as defined in 'default settings' in
 *   hook_filter_info(). These should be combined with $filter->settings to
 *   define the form element defaults.
 * @param $filters
 *   The complete list of filter objects that are enabled for the given format.
 *
 * @return
 *   An array of form elements defining settings for the filter. Array keys
 *   should match the array keys in $filter->settings and $defaults.
 */
function _mediawiki_api_filter_settings($form, &$form_state, $filter, $format, $defaults, $filters) {
  $elements = array();

  $elements['mediawiki_api_filter_url'] = array(
    '#type' => 'textfield',
    '#title' => t('MediaWiki API url'),
    '#default_value' => isset($filter->settings['mediawiki_api_filter_url']) ? $filter->settings['mediawiki_api_filter_url'] : '',
    '#description' => t('The system will call this url to generate the HTML code.'),
  );


  return $elements;
}